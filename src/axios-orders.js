import axios from 'axios';

const instance =axios.create({
    baseURL: 'https://hw-69-nia.firebaseio.com/'
});

export default instance;