import React, {Component, Fragment} from 'react';
import './ProductCard.css';
import {addProduct} from "../../store/actions/actions";
import {connect} from 'react-redux';
import {addToCart, delFromCart} from "../../store/actions/checkout";
import Spinner from "../UI/Spinner/Spinner";

class ProductCard extends Component {

    componentDidMount() {
        this.props.addProduct();
    };
    render() {
        return (
            <Fragment>
                {this.props.loading ? <Spinner/> : Object.keys(this.props.dishes).map((dish, index) => {
                    return (
                        <div key={index} className='ProductCard'>
                            <h3>{dish}</h3>
                            <img className='ProductImg' src={this.props.dishes[dish].Img} alt=""/>
                            <p>{this.props.dishes[dish].Cost} KGS</p>
                            <button onClick={()=> this.props.addToBasket(dish,this.props.dishes[dish].Cost)}>Order</button>
                        </div>
                    )
                })}
                <div className='Cart'>
                    {Object.keys(this.props.dishesForCart).map((dish,key)=>{
                        return <div key={key}>
                            <p>{dish} X{this.props.dishesForCart[dish]}</p>
                        </div>
                    })}
                </div>
            </Fragment>
        );
    }
}
const mapStateToProps =(state)=>({
dishes: state.menu.dishes || null,
dishesForCart: state.make.dishes,
loading:state.menu.loading,
});
const mapDispatchToProps =(dispatch)=>({
    addProduct: () => dispatch(addProduct()),
    addToBasket: (name,price) => dispatch(addToCart(name,price)),
    delFromCart: (name,price) => dispatch(delFromCart(name,price)),

});
export default connect(mapStateToProps, mapDispatchToProps)(ProductCard);