import React, {Component} from 'react';
import Spinner from "../Spinner/Spinner";
import {connect} from "react-redux";
import axios from "axios";
import {Redirect} from "react-router-dom";
import {createOrder} from "../../../store/actions/actions";


class Modal extends Component {
    state ={
        name:'',
        street:'',
        number:'',
        loading: false,
    };

    valueChanged = event =>{
        const {name,value} = event.target;
        this.setState({[name]:value});
    };
    orderHandler = event =>{
        event.preventDefault();

        const order ={
            dishes: this.props.dishes,
            price:this.props.price,
            customer:{...this.state}
        };
        this.setState({loading:true});

        axios.post('orders.json',order).finally(()=>{
            this.setState({loading:false},()=>{
                this.props.history.push('/');
            });
        });
    };
    render() {
        let form =(
            <form onSubmit={this.orderHandler}>
                <input type="text" placeholder='Your name' name='name'
                       value={this.state.name} onChange={this.valueChanged}/>
                <input type="text" placeholder='Your address' name='street'
                       value={this.state.street} onChange={this.valueChanged}/>
                <input type="text" placeholder='Your number' name='number'
                       value={this.state.number} onChange={this.valueChanged}/>
                <button>Make Order</button>
            </form>
        );
        if (this.props.loading){
            form =<Spinner/>;
        }
        if (this.props.ordered){
            form = <Redirect to='/'/>
        }
        return (
            <div className="Modal">
                <h4>Enter your contact data:</h4>
                {form}
            </div>
        );
    }
}
const mapStateToProps = state =>({
    dishes: state.bb.order,
    price: state.bb.totalPrice,
    loading:state.order.loading,
    ordered: state.order.ordered,
});
const mapDispatchToProps = dispatch=>({
    createOrder: order => dispatch(createOrder(order))
})
export default connect(mapStateToProps,mapDispatchToProps)(Modal);