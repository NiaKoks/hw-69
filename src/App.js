import React, { Component } from 'react';
import './App.css';
import ProductCard from './Components/Product/ProductCard'
import OrderForm from './Components/Order/OrderForm'
class App extends Component {
  render() {
    return (
      <div className="App">
        <ProductCard></ProductCard>
        <OrderForm></OrderForm>
      </div>
    );
  }
}

export default App;
