import {
    ORDER_REQUEST,
    ORDER_SUCCESS,
    ORDER_FAILURE,
} from '../actions/action-types'

const initialState ={
    dishes: {},
    loading: false,
    error: null,
};

const addReducerDish = (state=initialState,action) =>{
    switch (action.type) {
        case ORDER_REQUEST: {
            return{
                ...state,
                loading: true,
            }
        };
        case ORDER_SUCCESS: {
            return{
                ...state,
                loading:false,
                dishes: action.dishes,
            }
        };
        case ORDER_FAILURE: {
            return{
                ...state,
                loading:false,
                error: action.error
            }
        };
        default:
            return state;
    }
};

export default addReducerDish;