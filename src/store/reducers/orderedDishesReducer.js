import {
    ADD,
    DEL,
    INITDISH,
} from '../actions/action-types'

const INITIAL_DISH ={
    Dymlyama:0,
    Lepeshka:0,
    Plov:0,
    Samsa:0,
}

const initialState ={
    dishes: INITIAL_DISH,
    totalPrice:0,
    delivery: 150,
};
const orderedDishesReducer =(state=initialState,action)=>{
    switch (action.type) {
        case ADD:{
            return{
                ...state,
                dishes: {
                    ...state.dishes,
                    [action.name]: state.dishes[action.name] + 1,
                },
                totalPrice: state.totalPrice + action.price,
            }
        };
        case DEL:{
            if (state.dishes[action.name] >0){
                return{
                    ...state,
                    dishes: {
                        ...state.dishes,
                        [action.name]: state.dishes[action.name] -1,
                    },
                    totalPrice: state.totalPrice - action.price,
                }
            } else return state

        };
        case INITDISH:{
            return{
                ...state,
                dishes: {...INITIAL_DISH},
                totalPrice: state.totalPrice,
            }
        }

        default:
            return state;
    }
}

export default orderedDishesReducer;