import axios from '../../axios-orders';
import {ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS} from "./action-types";

export const orderReq = () =>({type: ORDER_REQUEST});
export const orderSucc = dishes => ({type: ORDER_SUCCESS,dishes});
export const orderFail = error =>({type: ORDER_FAILURE});

export const addProduct = product =>{
    return dispatch =>{
        dispatch(orderReq());
        axios.get('dishes.json').then(responce =>{
                dispatch(orderSucc(responce.data))
        },error => dispatch(orderFail(error))
        )
    }
};

